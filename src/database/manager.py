from sqlalchemy.orm import sessionmaker, clear_mappers, scoped_session
from sqlalchemy import inspect


class DatabaseManager:
    def __init__(self, engine):
        self.engine = engine
        self.session_manager = SessionManager(self.engine)
        self.mappings = []

    def begin_transaction(self):
        return self.engine.connect().begin()

    def get_engine(self):
        return self.engine

    def get_session_manager(self):
        return self.session_manager

    def clear_mappers(self):
        clear_mappers()

    def close(self):
        self.session_manager.close()
        self.engine.dispose()


class SessionManager(object):
    def __init__(self, engine):
        self.SessionMaker = scoped_session(sessionmaker(bind=engine))
        self.current_session = self.new_session()

    def new_session(self):
        self.current_session = self.SessionMaker()
        return self.get_session()

    def get_session(self):
        return self.current_session

    def get_status(self, entity):
        return inspect(entity)

    def close(self):
        self.SessionMaker.remove()
