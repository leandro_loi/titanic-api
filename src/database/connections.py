from sqlalchemy import create_engine

from exceptions.exceptions import DatabaseException


class ConnectionType(object):
    SQLITE = 'sqlite'


class ConnectionFactory(object):
    @staticmethod
    def get_connection(db_type, config):
        if db_type is ConnectionType.SQLITE:
            return SqlliteConnector.get_connection(config)
        else:
            raise DatabaseException('connection not found for %s', db_type)

    @staticmethod
    def get_uri(db_type, config):
        if db_type is ConnectionType.SQLITE:
            return SqlliteConnector.get_uri(config)
        else:
            raise DatabaseException('connection not found %s', db_type)


class SqlliteConnector(object):
    @staticmethod
    def get_uri(config):
        # return 'sqlite://'
        return 'sqlite:///{database}'.format(
            database=config['database']
        )

    @staticmethod
    def get_connection(config):
        return create_engine(SqlliteConnector.get_uri(config), case_sensitive=False)
