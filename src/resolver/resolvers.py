import pandas as pd

from exceptions.exceptions import SurvivorNotFoundException


class SurvivorsResolver:

    def __init__(self, name_service, model):
        self.name_service = name_service
        self.model = model

    def get_survived(self, name):
        resp = {}
        passanger = self.name_service.lookup_name(name=name)
        if passanger:

            passenger_attributes = pd.DataFrame([[passanger.sex, passanger.age, passanger.sibSp, passanger.parch,
                                                  passanger.embarked]])
            chances = self.model.chance_of_survive(passenger_attributes)
            survived = self.model.has_survived(chances)

            resp['survive'] = survived.get('survived')
            resp['probability'] = survived.get('probability')
            resp['probability_of_live'] = chances.get('1')
            resp['probability_of_death'] = chances.get('0')
            resp['matched_name'] = passanger.name
            resp['passenger_id'] = passanger.passengerId
        else:
            raise SurvivorNotFoundException()

        return resp

