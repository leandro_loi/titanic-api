from sqlalchemy import Column, Integer, Float, Text, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base

Model = declarative_base()


class Passenger(Model):

    __tablename__ = 'passengers'

    passengerId = Column(Integer, primary_key=True)
    survived = Column(Integer)
    pclass = Column(Integer)
    name = Column(Text)
    sex = Column(Integer)
    age = Column(Float)
    sibSp = Column(Integer)
    parch = Column(Integer)
    ticket = Column(Text)
    fare = Column(Float)
    cabin = Column(Text)
    embarked = Column(Integer)
    survivalChance = Column(Float)

    __table_args__ = (
        UniqueConstraint('passengerId', name='uc_passenger_id'),
    )






