import logging
from http import HTTPStatus

from flask import Blueprint
from flask_restful import Api, Resource
from werkzeug.exceptions import HTTPException

from src.api import container
from src.exceptions.exceptions import SurvivorNotFoundException

blueprint = Blueprint('survivors_blueprint', __name__)
api = Api()
api.init_app(blueprint)

survivors_repository = container['survivors_resolver']


class TitanicSurvivors(Resource):
    def get(self, name):
        """
        GET survive probability by name (Example: GET /survivor/name)
        :param name: Name of the passenger
        :return: JSON (Example:
            {
                "status": "predicted",
                "passenger_id": 1,
                "survive_probability": "0.8"
            }
        )
        """
        survivor_data = {
            'success': False,
            'message': 'Passenger not found'
        }
        try:
            analyse = survivors_repository.get_survived(name)
            survivor_data['success'] = True
            survivor_data['requested_name'] = name
            survivor_data['matched_name'] = analyse['matched_name']
            survivor_data['passenger_id'] = analyse['passenger_id']
            survivor_data['probability'] = analyse['probability']
            survivor_data['survived'] = analyse['survive']

            return survivor_data, HTTPStatus.OK
        except SurvivorNotFoundException as e:
            logging.error(e)
            return survivor_data, HTTPStatus.NOT_FOUND
        except HTTPException as e:
            logging.error(e)
            return survivor_data, HTTPStatus.BAD_REQUEST
        except Exception as e:
            logging.error(e)
            return survivor_data, HTTPStatus.INTERNAL_SERVER_ERROR


api.add_resource(TitanicSurvivors, '/survivor/<string:name>')