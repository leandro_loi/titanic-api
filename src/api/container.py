import logging

from rumple import Container

from database.connections import ConnectionFactory, ConnectionType
from model.models import ModelFactory, ModelsType
from model.manager import ModelManager
from database.manager import DatabaseManager
from repository.repositories import PassengersRepository
from resolver.resolvers import SurvivorsResolver
from services.name_lookup import NameLookup


class ApiContainer(object):
    __slots__ = ('config', 'container')

    def __init__(self, config):
        # type: (dict) -> None
        self.config = config
        self.container = Container()
        self.initialize()

    def initialize(self):
        self.initialize_logging()
        self.initialize_repositories()
        self.initialize_services()
        self.initialize_resolver()

    def get_logging_level(self):
        if self.config['logging']['level'] == 'info':
            return logging.INFO
        if self.config['logging']['level'] == 'debug':
            return logging.DEBUG
        return logging.WARNING

    def initialize_logging(self):
        logging.getLogger("pika").setLevel(logging.WARNING)
        logging.getLogger("requests.packages.urllib3").setLevel(logging.WARNING)

        logger = logging.getLogger()
        logger.setLevel(self.get_logging_level())

        handler = logging.StreamHandler()
        # if self.config['logging']['handler'] == 'file':
        #     handler = logging.FileHandler(self.config['logging']['log_path'])

        formatter = logging.Formatter()
        handler.setFormatter(formatter)
        logger.addHandler(handler)

    def initialize_repositories(self):
        connection_factory = ConnectionFactory.get_connection(ConnectionType.SQLITE,
                                                              self.config[ConnectionType.SQLITE])
        self.container['database'] = lambda: DatabaseManager(connection_factory)

        self.container['passengers_repository'] = lambda: PassengersRepository(self.container['database'])

        model_factory = ModelFactory.get_model(ModelsType.REGRESSION, self.config['model'])
        self.container['survivors_predictor'] = lambda: ModelManager(model_factory,
                                                                     self.config['model']['training_file'])
        self.container['survivors_predictor'].initialize()

    def initialize_services(self):
        self.container['name_lookup'] = lambda: NameLookup(self.container['passengers_repository'])

    def get(self):
        return self.container

    def initialize_resolver(self):
        self.container['survivors_resolver'] = lambda: SurvivorsResolver(self.container['name_lookup'],
                                                                         self.container['survivors_predictor'] )
