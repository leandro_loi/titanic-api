import os
from config.ConfigurationLoader import ConfigurationLoader
from api.container import ApiContainer

loader = ConfigurationLoader( os.path.dirname(os.path.realpath(__file__)) + '/../../config')
config = loader.load_configs('config.yml')
container = ApiContainer(config).get()