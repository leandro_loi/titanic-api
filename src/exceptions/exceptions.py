class BaseTitanicException(Exception):
    pass


class SurvivorNotFoundException(BaseTitanicException):
    pass


class DatabaseException(BaseTitanicException):
    pass


class ModelException(BaseTitanicException):
    pass
