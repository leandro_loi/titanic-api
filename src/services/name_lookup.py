
class NameLookup:

    def __init__(self, passengers_names):
        self.passengers_names = passengers_names

    def lookup_name(self, name):
        return self.passengers_names.find_passenger_by_similar_name(name)