from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression

from exceptions.exceptions import ModelException


class ModelsType(object):
    REGRESSION = 'LogisticRegression'
    RANDOMFOREST = 'RandomForestClassifier'


class ModelFactory:

    @staticmethod
    def get_model(model_type, config):
        if model_type is ModelsType.RANDOMFOREST:
            return RandomForestClassifier()
        elif model_type is ModelsType.REGRESSION:
                return LogisticRegression()
        else:
            raise ModelException('model_files not found for %s', model_type)
