import pandas as pd


class ModelManager:

    def __init__(self, engine, training_file):
        self.engine = engine
        self.training_file = training_file
        self.model = None
        self.df = None

    def initialize(self):
        self.load_files()
        self.normalize_age()
        self.normalize_sex()
        self.normalize_embarked()
        self.train_model()

    def load_files(self):
        self.df = pd.read_csv(self.training_file)
        self.df['survivalChance'] = float('NaN')

    def normalize_sex(self):
        self.df["Sex"][self.df["Sex"] == "male"] = 0
        self.df["Sex"][self.df["Sex"] == "female"] = 1

    def normalize_age(self):
        self.df["Age"] = self.df["Age"].fillna(self.df["Age"].median())

    def normalize_embarked(self):
        # Impute the Embarked variable
        self.df["Embarked"] = self.df['Embarked'].fillna('S')
        # Convert the Embarked classes to integer form
        self.df["Embarked"][self.df["Embarked"] == "S"] = 0
        self.df["Embarked"][self.df["Embarked"] == "C"] = 1
        self.df["Embarked"][self.df["Embarked"] == "Q"] = 2

    def get_target(self):
        return self.df['Survived'].values

    def get_features(self):
        return self.df[["Sex", "Age", "SibSp", "Parch", 'Embarked']].values

    def train_model(self):
        self.model = self.engine.fit(self.get_features(), self.get_target())

    def predict(self, features):
        predicted = self.model.predict(features)
        survived = float(predicted[0])
        return survived

    def chance_of_survive(self, features):
        prob = self.model.predict_proba(features)
        resp = {
            '0': prob[0][0],
            '1': prob[0][1],
        }
        return resp

    def has_survived(self, probability):
        resp = {}
        if probability.get('0') > probability.get('1'):
            resp['survived'] = 'NO'
            resp['probability'] = probability.get('0')
        else:
            resp['survived'] = 'YES'
            resp['probability'] = probability.get('1')

        return resp
