from entity.entities import Passenger


class PassengersRepository:
    def __init__(self, database):
        self.database = database
        self.db_session = database.get_session_manager()

    def find_passenger_by_name(self, passenger_name):
        # type: (str) -> Passenger
        passenger = self.db_session.get_session().query(Passenger) \
            .filter(Passenger.name == passenger_name) \
            .first()

        return passenger

    def find_passenger_by_similar_name(self, passenger_name):
        # type: (str) -> Passenger
        passenger = self.db_session.get_session().query(Passenger) \
            .filter(Passenger.name.ilike('%' + passenger_name+ '%')) \
            .first()

        return passenger

    def find_passenger_by_id(self, passenger_id):
        # type: (str) -> Passenger
        passenger = self.db_session.get_session().query(Passenger) \
            .filter(Passenger.passengerId == passenger_id) \
            .first()

        return passenger

