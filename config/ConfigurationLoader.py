import yaml
import logging


class ConfigurationLoader:

    def __init__(self, path):
        self.path = path

    def load_configs(self, filename):
        with open(self.path + '/' + filename) as s:
            try:
                return yaml.load(s)
            except yaml.YAMLError as exc:
                logging.error(exc)