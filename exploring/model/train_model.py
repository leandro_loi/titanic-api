from sqlalchemy import create_engine
import pandas as pd
from sklearn.linear_model import LogisticRegression


class BuildModel:

    def __init__(self, filename):
        self.filename = filename
        self.df = None
        self.initialize()
        self.model = None

    def initialize(self):
        self.load_files()
        self.normalize_age()
        self.normalize_sex()
        self.normalize_embarked()

    def load_files(self):
        self.df = pd.read_csv(self.filename)
        self.df['survivalChance'] = float('NaN')
        print()

    def normalize_sex(self):
        self.df["Sex"][self.df["Sex"] == "male"] = 0
        self.df["Sex"][self.df["Sex"] == "female"] = 1

    def normalize_age(self):
        self.df["Age"] = self.df["Age"].fillna(self.df["Age"].median())

    def normalize_embarked(self):
        # Impute the Embarked variable
        self.df["Embarked"] = self.df['Embarked'].fillna('S')
        # Convert the Embarked classes to integer form
        self.df["Embarked"][self.df["Embarked"] == "S"] = 0
        self.df["Embarked"][self.df["Embarked"] == "C"] = 1
        self.df["Embarked"][self.df["Embarked"] == "Q"] = 2

    def get_target(self):
        return self.df['Survived'].values

    def get_features(self):
        return self.df[["Sex", "Age", "SibSp", "Parch", 'Embarked']].values

    def get_df_to_save(self):
        pass

    def save_db(self, path):
        disk_engine = create_engine('sqlite:///' + path)
        self.df.to_sql('passengers', disk_engine, if_exists='append')

    def train_model(self):
        model = LogisticRegression()
        self.model = model.fit(self.get_features(), self.get_target())

    def predict(self, features):
        predicted = self.model.predict(features)
        return predicted[0]

    def chance_of_survive(self, features):
        return self.model.predict_proba(features)

    def print_est_score(self):
        print("Best score is {}".format(self.model.best_score_))



if __name__ == "__main__":

    train = BuildModel('res/train.csv')
    train.train_model()
    train.print_est_score()
    exit(1)

    # train.save_db('../db/passengers.db')
    test = BuildModel('res/test.csv')

    passenger = test.df[["Sex", "Age", "SibSp", "Parch", 'Embarked']].values
    passenger = pd.DataFrame([[1, 35, 1, 0, 1]])
    predicted_result = train.predict(passenger)

    print(predicted_result)

