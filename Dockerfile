# use base python image with python 3
FROM frolvlad/alpine-python-machinelearning

# Add deps
ADD requirements.txt /requirements.txt

RUN set -ex \
    && apk --no-cache add --virtual .build-deps \
            gcc \
            make \
            libc-dev \
            musl-dev \
            linux-headers \
            python3-dev \
            build-base

RUN pip install -r /requirements.txt

# create working dir
ADD application /application
# set working directory to /application/
WORKDIR /application
ENV PYTHONPATH /application:/application/src/
CMD [ "python", "/application/bin/titanic-api.py" ]
