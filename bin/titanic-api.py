import os
import sys
from flask import Flask

from api.blueprint import survivor

src_dir = os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'src'))
sys.path.insert(0, src_dir)


# Initialize Flask
app = Flask(__name__)
app.register_blueprint(survivor.blueprint)
app.config['ERROR_404_HELP'] = False

if __name__ == '__main__':
    app.run(host='0.0.0.0')
