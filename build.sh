#! /bin/bash

function cleanup {
  rm -rf git_*
  rm -rf application
}

trap cleanup SIGHUP SIGINT SIGTERM

git clone -b master --single-branch https://leandro_loi@bitbucket.org/leandro_loi/titanic-api.git application

docker build -t titanic-api:v1.0.1 .

cleanup

