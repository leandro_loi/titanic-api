import unittest

from mock import Mock
from model.manager import ModelManager


class TestModel(unittest.TestCase):
    def setUp(self):
        self.engine = Mock()
        self.training_file = Mock()
        self.model_manager = ModelManager(self.engine, self.training_file)

    def test_predict(self):
        features = Mock(Sex=1, Age=30, SibSp=0, Parch=0, Embarked=1)
        self.model_manager.model = Mock()
        self.model_manager.model.predict.return_value = [1]
        resp = self.model_manager.predict(features)
        self.assertEqual(1.0, resp)

    def test_chance_of_survive(self):
        features = Mock(Sex=1, Age=30, SibSp=0, Parch=0, Embarked=1)
        self.model_manager.model = Mock()
        self.model_manager.model.predict_proba.return_value = [[0.7, 0.3]]
        resp = self.model_manager.chance_of_survive(features)
        self.assertEqual(0.7, resp['0'])
        self.assertEqual(0.3, resp['1'])

