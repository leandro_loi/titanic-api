import unittest

from mock import Mock
from services.name_lookup import NameLookup


class TestServicesNameLookup(unittest.TestCase):
    def setUp(self):
        self.passengers_names = Mock()
        self.name_lookup = NameLookup(self.passengers_names)

    def test_get_survived(self):
        passanger = Mock(age=32.0, cabin='D15', embarked=1, fare=76.2917, pname='Bazzani, Miss. Albina',
                         parch=0, passengerId=219, pclass=1, sex=1, sibSp=0, ticket=11813)
        self.passengers_names.find_passenger_by_similar_name.return_value = passanger

        resp = self.name_lookup.lookup_name('albina')

        self.assertEqual(32.0, resp.age)
        self.assertEqual('D15', resp.cabin)
        self.assertEqual(1, resp.embarked)
        self.assertEqual(76.2917, resp.fare)
        self.assertEqual(0, resp.parch)
        self.assertEqual(219, resp.passengerId)
        self.assertEqual(1, resp.pclass)
        self.assertEqual(1, resp.sex)
        self.assertEqual(0, resp.sibSp)
        self.assertEqual(11813, resp.ticket)
        self.assertEqual('Bazzani, Miss. Albina', resp.pname)



