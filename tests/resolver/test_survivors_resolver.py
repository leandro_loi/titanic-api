import unittest

from mock import Mock
from resolver.resolvers import SurvivorsResolver


class TestSurvivorsResolver(unittest.TestCase):
    def setUp(self):
        self.name_service = Mock()
        self.model = Mock()
        self.suvivor_resolver = SurvivorsResolver(self.name_service, self.model)

    def test_get_survived(self):
        passanger = Mock(age=32.0, cabin='D15', embarked=1, fare=76.2917, name='Bazzani, Miss. Albina',
                         parch=0, passengerId=219, pclass=1, sex=1, sibSp=0, ticket=11813)
        chance_of_survive = {'0': 0.7, '1': 0.3}
        has_survived = {'survived': 'NO', 'probability': 0.7}
        self.name_service.lookup_name.return_value = passanger
        self.model.chance_of_survive.return_value = chance_of_survive
        self.model.has_survived.return_value = has_survived
        resp = self.suvivor_resolver.get_survived('albina')

        self.assertEqual('NO', resp['survive'])
        self.assertEqual(0.7, resp['probability'])
        self.assertEqual(0.3, resp['probability_of_live'])
        self.assertEqual(0.7, resp['probability_of_death'])
        self.assertEqual(219, resp['passenger_id'])

