
<p align="center">
  <a href="https://www.finiata.com/">
    <img width="240" src="https://finiata.wpengine.com/wp-content/uploads/2017/06/finiata-pos-rgb_blue.png">
  </a>
</p>

# Titanic API - Finiata

Develop this test was really fun, no just the part of build the model, but to develop the API it self.

Thanks for this opportunity.

##### About the project:

1. Build a simple model (of your choice) to predict ‘survival’ based on ‘sex’, ‘age’. ‘sibsp’, ‘parch’ and ‘embarked’. Predictive accuracy of this model won’t be judged. 

     - The choice - I made two tests. The first one was with RandomForestClassifier and the other one was LogisticRegression, but is configurable on the config.yml file which one should be used.
     
     - By default I left RandomForestClassifier.

2. We now want to deploy this model as a Flask API. The API should take ‘Name’ as a get parameter and return both the passenger that matches the name most closely as well as the survival prediction for this particular person. Specifically

    - Here is the existing endpoints:
    
| Method | Endpoint                                  | Description                                                           |
|--------|-------------------------------------------|-----------------------------------------------------------------------|
| GET    | /survival/:name                           | get passenger information and change of survival                      |

3. Implement a lookup function that takes as input a name, finds the closest matching passenger in the training data and returns ‘sex’, ‘age’,‘sibsp’, ‘parch’ and ‘embarked’ for the matched record.

    - The lookup decision was based on the database to be able to scale. I made a search by like in the names of the passengers. But other idea could be use Levenshtein distance method in order to find the closest name from the passanger list.
    
4. Predict the chance of survival for that particular passenger using the model you build in 2.

    - In order to do this I used the `predict_proba` in the model.
5. Let the API return both the matched name as well as the prediction for that name.
    
    - The result of the API is a json:
    
    ```json
        {
            "success": true,
            "message": "Passenger not found",
            "requested_name": "Philip",
            "matched_name": "Kiernan, Mr. Philip",
            "passenger_id": 215,
            "probability": 0.7907501038289546,
            "survived": "NO"
        }
    ```
    - Invalid names will return like this:
    ```json
        {
            "success": false,
            "message": "Passenger not found"
        }
    
    ```

##### Running the docker container:
```commandline
docker run -d -p 5000:5000 titanic-api:v1.0.0
```




